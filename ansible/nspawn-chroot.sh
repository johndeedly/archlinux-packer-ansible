#!/usr/bin/env bash

# prepare nspawn environment to allow ufw firewall configuration
modprobe iptable_filter
modprobe ip6table_filter

# https://www.enricozini.org/blog/2021/debian/nspawn-chroot-maintenance/
chroot="$1"
shift
exec systemd-nspawn --capability=CAP_SYS_CHROOT,CAP_NET_ADMIN,CAP_NET_RAW --link-journal=host --console=pipe -qD "$chroot" -- "$@"
