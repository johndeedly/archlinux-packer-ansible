packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}


variable "yearmonthday" {
  type = string
}

variable "sound_driver" {
  type = string
}

variable "accel_graphics" {
  type = string
}

variable "cpu_cores" {
  type    = number
  default = 4
}

variable "memory" {
  type    = number
  default = 3072
}

variable "encryption" {
  type    = bool
  default = false
}

variable "dualboot" {
  type    = bool
  default = false
}

variable "headless" {
  type    = bool
  default = true
}

variable "pxe" {
  type    = bool
  default = false
}

variable "stage" {
  type    = string
  default = "bootstrap"
}

variable "configuration" {
  type    = list(string)
  default = [
    "verbose",
    "build_archiso",
    #"target_host",
    "target_guest",
    #"target_container",
    #"target_nspawn",
    "bootstrap",
    "encryption",
    #"dualboot",
    #"pxeboot",
  ]
}


source "qemu" "default" {
  shutdown_command   = "/sbin/poweroff"
  cd_files           = ["CIDATA/*"]
  cd_label           = "CIDATA"
  disk_size          = 524288
  memory             = var.memory
  format             = "qcow2"
  accelerator        = "kvm"
  disk_discard       = "unmap"
  disk_detect_zeroes = "unmap"
  disk_interface     = "virtio"
  disk_compression   = false
  skip_compaction    = true
  net_device         = "virtio-net"
  vga                = "virtio"
  machine_type       = "q35"
  cpu_model          = "host"
  vtpm               = true
  tpm_device_type    = "tpm-tis"
  efi_boot           = true
  efi_firmware_code  = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars  = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets            = 1
  cores              = var.cpu_cores
  threads            = 1
  qemuargs           = [["-rtc", "base=utc,clock=host"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-virtfs", "local,path=output,mount_tag=host.0,security_model=mapped,id=host.0"]]
  headless           = var.headless
  iso_checksum       = "none"
  iso_url            = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory   = "output/${var.stage}"
  ssh_username       = "root"
  ssh_password       = "packer-build-passwd"
  ssh_timeout        = "10m"
  vm_name            = "archlinux-${var.stage}-${var.yearmonthday}-x86_64"
}


build {
  sources = ["source.qemu.default"]

  # mount the external output folder as share
  provisioner "shell" {
    inline = [
      "mount --mkdir -t 9p -o trans=virtio,version=9p2000.L,rw host.0 /share || true"
    ]
  }

  # make the journal log persistent on ramfs
  provisioner "shell" {
    inline = [
      "mkdir -p /var/log/journal",
      "systemd-tmpfiles --create --prefix /var/log/journal",
      "systemctl restart systemd-journald"
    ]
  }

  # prepare ansible in archiso environment
  provisioner "shell" {
    inline = [
      "mount -o remount,size=75% /run/archiso/cowspace || true",
      "pacman -Syy --needed --noconfirm --color=auto ansible python-pip python-setuptools python-wheel python-lxml"
    ]
  }

  provisioner "ansible-local" {
    playbook_dir    = "ansible"
    playbook_file   = "ansible/site.yml"
    inventory_file  = "ansible/inventory.yml"
    extra_arguments = ["--tags", "${join(",", var.configuration)}"]
  }

  provisioner "shell-local" {
    inline = [<<EOS
tee output/bootstrap/archlinux-bootstrap-${var.yearmonthday}-x86_64.run.sh <<EOF
#!/usr/bin/env bash
trap "trap - SIGTERM && kill -- -\$\$" SIGINT SIGTERM EXIT
mkdir -p "/tmp/swtpm.0" "share"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name archlinux-bootstrap-${var.yearmonthday}-x86_64 \\
  -machine type=q35,accel=kvm \\
  -vga virtio \\
  -cpu host \\
  -drive file=archlinux-bootstrap-${var.yearmonthday}-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -smp ${var.cpu_cores},sockets=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user.0 -device virtio-net,netdev=user.0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -virtfs local,path=share,mount_tag=host.0,security_model=mapped,id=host.0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
# -display none, -daemonize, hostfwd=::12345-:22 for running as a daemonized server
chmod +x output/bootstrap/archlinux-bootstrap-${var.yearmonthday}-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
  }
}
