packer {
  required_plugins {
    virtualbox = {
      source  = "github.com/hashicorp/virtualbox"
      version = "~> 1"
    }
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}


variable "yearmonthday" {
  type = string
}

variable "sound_driver" {
  type = string
}

variable "accel_graphics" {
  type = string
}

variable "cpu_cores" {
  type    = number
  default = 4
}

variable "memory" {
  type    = number
  default = 3072
}

variable "encryption" {
  type    = bool
  default = false
}

variable "dualboot" {
  type    = bool
  default = false
}

variable "headless" {
  type    = bool
  default = true
}

variable "pxe" {
  type    = bool
  default = false
}

variable "stage" {
  type    = string
  default = "bootstrap"
}

variable "configuration" {
  type    = list(string)
  default = [
    "verbose",
    "build_archiso",
    #"target_host",
    "target_guest",
    #"target_container",
    #"target_nspawn",
    "bootstrap",
    "encryption",
    #"dualboot",
    #"pxeboot",
  ]
}


source "virtualbox-iso" "default" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/${var.stage}"
  output_filename          = "../archlinux-${var.stage}-${var.yearmonthday}-x86_64"
  ssh_username             = "root"
  ssh_password             = "packer-build-passwd"
  ssh_timeout              = "10m"
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "${var.cpu_cores}", "--audio-driver", "${var.sound_driver}", "--audio-out", "on", "--audio-enabled", "on", "--usb", "on", "--usb-xhci", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "on", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto"]]
  vboxmanage_post          = [["modifyvm", "{{ .Name }}", "--macaddress1", "auto"]]
  vm_name                  = "archlinux-${var.stage}-${var.yearmonthday}-x86_64"
}


build {
  sources = ["source.virtualbox-iso.default"]

  # mount the external output folder as share
  provisioner "shell" {
    inline = [
      "mount --mkdir -t 9p -o trans=virtio,version=9p2000.L,rw host.0 /share || true"
    ]
  }

  # make the journal log persistent on ramfs
  provisioner "shell" {
    inline = [
      "mkdir -p /var/log/journal",
      "systemd-tmpfiles --create --prefix /var/log/journal",
      "systemctl restart systemd-journald"
    ]
  }

  # prepare ansible in archiso environment
  provisioner "shell" {
    inline = [
      "mount -o remount,size=75% /run/archiso/cowspace || true",
      "pacman -Syy --needed --noconfirm --color=auto ansible python-pip python-setuptools python-wheel python-lxml"
    ]
  }

  provisioner "ansible-local" {
    playbook_dir    = "ansible"
    playbook_file   = "ansible/site.yml"
    inventory_file  = "ansible/inventory.yml"
    extra_arguments = ["--tags", "${join(",", var.configuration)}"]
  }
}
